<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/new',[
    'uses' => 'PagesController@new'
]);

Route::get('/todos', [
    'uses' => 'TodosController@index',
    'as' => 'todos'
]);

Route::get('/delete/todo/{id}',[
    'uses' => 'TodosController@delete',
    'as' => 'todo_delete'
]);

Route::get('/update/todo/{id}',[
    'uses' => 'TodosController@update',
    'as' => 'todo_update'
]);

Route::get('todo/completed/{id}', [
    'uses' => 'TodosController@completed',
    'as' => 'todo_completed'
]);

Route::post('/todo/save/{id}',[
    'uses' => 'TodosController@save',
    'as' => 'todo_save'
]);

Route::post('/create/todo', [
    'uses' => 'TodosController@store'
]);